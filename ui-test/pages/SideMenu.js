module.exports = {
  elements: {
    sideMenuButton: { selector: '[aria-label="Open Sidenav"]'},
    aboutUsPage: { selector: '[routerlink="/about"]'}
  },
  commands: [{
    openSideMenu: function() {
      this
        .log(`Opening side menu`)
        .waitForElementPresent('@sideMenuButton')
        .click('@sideMenuButton')
        .pause(1000);

      return this.api;
    },
    openAboutUs: function() {
      this
        .log(`Opening About us page`)
        .waitForElementPresent('@aboutUsPage')
        .click('@aboutUsPage');

      return this.api;
    }
  }]
};
