module.exports = {
  elements: {
    accountMenuButton: { selector: '#navbarAccount'},
    loginButton: { selector: '#navbarLoginButton'},
    logoutButton: { selector: '#navbarLogoutButton'}
  },
  commands: [{
    clickAccountMenu: function() {
      this
        .log(`Clicking Account menu button`)
        .waitForElementPresent('@accountMenuButton')
        .click('@accountMenuButton')
        .pause(1000);

      return this.api;
    },
    clickLogin: function() {
      this
        .log(`Clicking Login button`)
        .waitForElementPresent('@loginButton')
        .click('@loginButton');

      return this.api;
    },
    clickLogout: function() {
      this
        .log(`Clicking Logout button`)
        .waitForElementPresent('@logoutButton')
        .click('@logoutButton');

      return this.api;
    }
  }]
};
