module.exports = {
  elements: {
    welcomeBannerCloseButton: { selector: '[aria-label="Close Welcome Banner"]'},
    registerButton: { selector: '[href="#/register"]'},
    emailInput: { selector: '#emailControl'},
    passwordInput: { selector: '#passwordControl'},
    repeatPasswordInput: { selector: '#repeatPasswordControl'},
    securityQuestionSelect: { selector: 'div.security-container > mat-form-field > div > div'},
    securityQuestionOption: { selector: 'mat-option[role="option"]'},
    securityQuestionInput: { selector: '#securityAnswerControl'},
    submitNewUser: { selector: '#registerButton'},
    loginEmail: { selector: '#email'},
    loginPassword: { selector: '#password'},
    loginSubmit: { selector: '#loginButton'},
    basketButton: { selector: '[aria-label="Show the shopping cart"]'},
    basketItems: { selector: 'app-purchase-basket  mat-header-row'},
    checkoutButton: { selector: '#checkoutButton'},
    addAddressButton: { selector: '[routerlink="/address/create"]'},
    country: { selector: '[data-placeholder="Please provide a country."]'},
    name: { selector: '[data-placeholder="Please provide a name."]'},
    phone: { selector: '[data-placeholder="Please provide a mobile number."]'},
    address: { selector: '[data-placeholder="Please provide an address."]'},
    zip: { selector: '[data-placeholder="Please provide a ZIP code."]'},
    city: { selector: '[data-placeholder="Please provide a city."]'},
    state: { selector: '[data-placeholder="Please provide a state."]'},
    addressSubmit: { selector: '#submitButton'},
    addressRadioButton: { selector: 'mat-radio-button'},
    continueToPayment: { selector: '[aria-label="Proceed to payment selection"]'},
    deliverySpeedButton: { selector: 'mat-radio-button'},
    proceedToDelivery: { selector: '[aria-label="Proceed to delivery method selection"]'},
    addNewCard: { selector: 'mat-expansion-panel'},
    cardName: { locateStrategy: 'xpath', selector: '//mat-label[text() = "Name"]/ancestor::mat-form-field//input'},
    cardNumber: { locateStrategy: 'xpath', selector: '//mat-label[text() = "Card Number"]/ancestor::mat-form-field//input'},
    expiryMonth: { locateStrategy: 'xpath', selector: '//mat-label[text() = "Expiry Month"]/ancestor::mat-form-field//select'},
    expiryYear: { locateStrategy: 'xpath', selector: '//mat-label[text() = "Expiry Year"]/ancestor::mat-form-field//select'},
    cardSubmit: { selector: '#submitButton'},
    cardRadioButton: { selector: 'mat-radio-button'},
    proceedToReview: { selector: '[aria-label="Proceed to review"]'},
    checkout: { selector: '#checkoutButton'},
    title: { selector: 'h1'},
    pageTitle: { selector: 'button[aria-label="Back to homepage"] span span'},
    languageButon: { selector: '[aria-label="Language selection menu"]'},
    deLang: { selector: 'span.flag-icon-de'},
    enLang: { selector: 'span.flag-icon-us'},
    lastLoginIp: { selector: 'app-last-login-ip p small'},
    profileEmail: { selector: 'input[type="email"]'},
    productsGrid: { selector: 'mat-grid-list'}
  },
  commands: [{
    closeWelcomeBanner: function() {
      this
        .log(`Closing welcome banner`)
        .waitForElementPresent('@welcomeBannerCloseButton')
        .click('@welcomeBannerCloseButton');

      return this.api;
    },
    clickRegister: function() {
      this
        .log(`Clicking Register button`)
        .waitForElementPresent('@registerButton')
        .click('@registerButton');

      return this.api;
    },
    registerNewUser: function(user) {
      this
        .log(`Registering new user`)
        .waitForElementPresent('@emailInput')
        .setValue('@emailInput', user.email)
        .setValue('@passwordInput', user.password)
        .setValue('@repeatPasswordInput', user.password)
        .pause(1000)
        .click('@securityQuestionSelect')
        .pause(1000)
        .waitForElementPresent('@securityQuestionOption')
        .click('@securityQuestionOption')
        .pause(1000)
        .setValue('@securityQuestionInput', user.securityQuestion)
        .click('@submitNewUser');

      return this.api;
    },
    login: function(user) {
      this
        .log(`Login with user details`)
        .waitForElementPresent('@loginEmail')
        .setValue('@loginEmail', user.email)
        .setValue('@loginPassword', user.password)
        .click('@loginSubmit')
        .pause(1000);

      return this.api;
    },
    addProductsByIndex: function(index) {
      this
        .log(`Add products to basket by index: ${index}`)
        .waitForElementPresent(`mat-grid-list > div > mat-grid-tile:nth-child(${index}) button[aria-label="Add to Basket"]`)
        .click(`mat-grid-list > div > mat-grid-tile:nth-child(${index}) button[aria-label="Add to Basket"]`)
        .pause(1000);

      return this.api;
    },
    openBasket: function() {
      this
        .log(`Open basket`)
        .waitForElementPresent(`@basketButton`)
        .click(`@basketButton`)
        .pause(1000);

      return this.api;
    },
    clickCheckout: function() {
      this
        .log(`Clicking checkout button`)
        .waitForElementPresent('@checkoutButton')
        .click('@checkoutButton')
        .pause(1000);

      return this.api;
    },
    clickAddAddress: function() {
      this
        .log(`Clicking add address button`)
        .waitForElementPresent('@addAddressButton')
        .click('@addAddressButton')
        .pause(1000);

      return this.api;
    },
    addNewAddress: function(address) {
      this
        .log(`Creating new delivery address`)
        .waitForElementPresent('@country')
        .setValue('@country', address.country)
        .setValue('@name', address.name)
        .setValue('@phone', address.phone)
        .setValue('@zip', address.zip)
        .setValue('@address', address.address)
        .setValue('@city', address.city)
        .setValue('@state', address.state)
        .click('@addressSubmit');

      return this.api;
    },
    selectDeliveryAddress: function() {
      this
        .log(`Selecting first delivery address`)
        .waitForElementPresent('@addressRadioButton')
        .click('@addressRadioButton')
        .pause(1000)
        .click('@continueToPayment');

      return this.api;
    },
    selectDeliverySpeed: function() {
      this
        .log(`Selecting first delivery speed`)
        .waitForElementPresent('@deliverySpeedButton')
        .click('@deliverySpeedButton')
        .pause(1000)
        .click('@proceedToDelivery');

      return this.api;
    },
    addNewPaymentCard: function(card) {
      this
        .log(`Adding new payment card`)
        .waitForElementPresent('@addNewCard')
        .click('@addNewCard')
        .pause(1000)
        .useXpath()
        .waitForElementPresent('@cardName')
        .setValue('@cardName', card.name)
        .setValue('@cardNumber', card.card)
        .click('@expiryYear')
        .pause(1000)
        .click(`//mat-label[text() = "Expiry Year"]/ancestor::mat-form-field//select/option[@value="${card.year}"]`)
        .click('@expiryMonth')
        .pause(1000)
        .click(`//mat-label[text() = "Expiry Month"]/ancestor::mat-form-field//select/option[@value="${card.month}"]`)
        .pause(1000)
        .useCss()
        .click(`@cardSubmit`)
        .click(`@cardRadioButton`);

      return this.api;
    },
    checkout: function() {
      this
        .log(`Checkout`)
        .waitForElementPresent('@proceedToReview')
        .click(`@proceedToReview`)
        .waitForElementPresent('@checkout')
        .click(`@checkout`)
        .assert.containsText('@title', 'Thank you for your purchase!');

      return this.api;
    },
    checkPageTile: function(title) {
      this
        .log(`Checking page title: ${title}`)
        .waitForElementPresent('@pageTitle')
        .assert.containsText('@pageTitle', title);

      return this.api;
    },
    checkContentTile: function(title) {
      this
        .log(`Checking page content title: ${title}`)
        .waitForElementPresent('@title')
        .assert.containsText('@title', title);

      return this.api;
    },
    checkProfileEmail: function(val) {
      this
        .log(`Checking profile page email ${val}`)
        .waitForElementPresent('@profileEmail')
        .assert.value('@profileEmail', val);

      return this.api;
    },
    changeLanguageToDeutsch: function() {
      this
        .log(`Change language to Deutsch`)
        .waitForElementPresent('@languageButon')
        .click(`@languageButon`)
        .pause(1000)
        .waitForElementPresent('@deLang')
        .click(`@deLang`);

      return this.api;
    },
    changeLanguageToEnglish: function() {
      this
        .log(`Change language to English`)
        .waitForElementPresent('@languageButon')
        .click(`@languageButon`)
        .pause(1000)
        .waitForElementPresent('@enLang')
        .click(`@enLang`);

      return this.api;
    },
    checkLastIp: function(val) {
      this
        .log(`Checking if last Ip is ` + val)
        .waitForElementPresent('@lastLoginIp')
        .assert.containsText('@lastLoginIp', val);

      return this.api;
    },
    checkIfProductsVisible: function() {
      this
        .log(`Checking if main page with product grid is visible`)
        .waitForElementPresent('@productsGrid');

      return this.api;
    }
  }]
};
