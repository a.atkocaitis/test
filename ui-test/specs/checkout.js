var newUser = {
  email: new Date().getTime() + '-juice-shop-test@mailinator.com',
  password: 'password12345',
  securityQuestion: 'Bob'
};

module.exports = {
  tags: ['test'],

  'Register as a new user' : function(browser) {
    browser
      .url(browser.globals.url)
      .page.HomePage().closeWelcomeBanner()
      .page.AccountMenu().clickAccountMenu()
      .page.AccountMenu().clickLogin()
      .page.HomePage().clickRegister()
      .page.HomePage().registerNewUser(newUser);
  },

  'Login with new user' : function(browser) {
    browser
      .page.HomePage().login(newUser);
  },

  'Add products to basker' : function(browser) {
    browser
      .page.HomePage().addProductsByIndex(1)
      .page.HomePage().addProductsByIndex(2);
  },

  'Checkout' : function(browser) {
    var address = {
      country: "USA",
      name: "Lorem Ipsum",
      phone: "12345678",
      zip: "12345",
      address: "Hollywood bl. 1",
      city: "LV",
      state: "Nevada"
    };

    var card = {
      name: "Lorem Ipsum",
      card: "1234123412341234",
      month: "1",
      year: "2080"
    };

    browser
      .page.HomePage().openBasket()
      .page.HomePage().clickCheckout()
      .page.HomePage().clickAddAddress()
      .page.HomePage().addNewAddress(address)
      .page.HomePage().selectDeliveryAddress()
      .page.HomePage().selectDeliverySpeed()
      .page.HomePage().addNewPaymentCard(card)
      .page.HomePage().checkout();
  }

};
