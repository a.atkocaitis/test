module.exports = {
  tags: ['test'],

  'Checking About Us page title' : function(browser) {
    browser
      .url(browser.globals.url)
      .page.HomePage().closeWelcomeBanner()
      .page.SideMenu().openSideMenu()
      .page.SideMenu().openAboutUs()
      .page.HomePage().checkContentTile('About Us')
      .page.HomePage().changeLanguageToDeutsch()
      .page.HomePage().checkContentTile('Über uns');
  }

};
