const ip = require('ip');
var newUser = {
  email: new Date().getTime() + '-juice-shop-test@mailinator.com',
  password: 'password12345',
  securityQuestion: 'Bob'
};

module.exports = {
  tags: ['test'],

  'Register as a new user' : function(browser) {
    browser
      .url(browser.globals.url)
      .page.HomePage().closeWelcomeBanner()
      .page.AccountMenu().clickAccountMenu()
      .page.AccountMenu().clickLogin()
      .page.HomePage().clickRegister()
      .page.HomePage().registerNewUser(newUser);
  },

  'Login with new user' : function(browser) {
    browser
      .page.HomePage().login(newUser);
  },

  'Logout from current user' : function(browser) {
    browser
      .page.AccountMenu().clickAccountMenu()
      .page.AccountMenu().clickLogout();
  },

  'Login with new user again' : function(browser) {
    browser
      .page.AccountMenu().clickAccountMenu()
      .page.AccountMenu().clickLogin()
      .page.HomePage().login(newUser);
  },

  'Checking Last login IP' : function(browser) {
    browser
      .url(browser.globals.url + '#/privacy-security/last-login-ip')
      .page.HomePage().checkLastIp(ip.address());
  }

};
