var newUser = {
  email: new Date().getTime() + '-juice-shop-test@mailinator.com',
  password: 'password12345',
  securityQuestion: 'Bob'
};

module.exports = {
  tags: ['test'],

  'Register as a new user' : function(browser) {
    browser
      .url(browser.globals.url)
      .page.HomePage().closeWelcomeBanner()
      .page.AccountMenu().clickAccountMenu()
      .page.AccountMenu().clickLogin()
      .page.HomePage().clickRegister()
      .page.HomePage().registerNewUser(newUser);
  },

  'Login with new user' : function(browser) {
    browser
      .page.HomePage().login(newUser);
  },

  'Check profile page email' : function(browser) {
    browser
      .url(browser.globals.url + '/profile')
      .page.HomePage().checkProfileEmail(newUser.email)
      .url(browser.globals.url);
  },

  'Logout from current user' : function(browser) {
    browser
      .page.AccountMenu().clickAccountMenu()
      .page.AccountMenu().clickLogout();
  },

  'Check if profile page redirects to home page' : function(browser) {
    browser
      .url(browser.globals.url + '/profile')
      .page.HomePage().checkIfProductsVisible();
  }

};
