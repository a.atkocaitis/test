module.exports = {
  tags: ['smoke'],

  'Open home page and check if main title is visible' : function(browser) {
    browser
      .url(browser.globals.url)
      .page.HomePage().checkPageTile('OWASP Juice Shop');
  }

};
